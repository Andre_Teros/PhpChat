<?php

require 'vendor/autoload.php';

use React\Socket\Connector;
use React\EventLoop\Factory;
use React\Socket\ConnectionInterface;
use React\Stream\ReadableResourceStream as RRS;
use React\Stream\WritableResourceStream as WRS;

$loop = Factory::create();
$input = new RRS(STDIN, $loop);
$output = new WRS(STDOUT, $loop);



$connector = new Connector($loop);
$connector->connect('127.0.0.1:8000')
    ->then(
        function (ConnectionInterface $connection) use ($input, $output) {

//            $input->on('data', function ($data) use ($connection) {
//                $connection->write($data);
//            });
            $input->pipe($connection);

//            $connection->on('data', function ($data) use ($output) {
//                $output->write($data);
//            });
            $connection->pipe($output);
        },
        function (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    );


$loop->run();