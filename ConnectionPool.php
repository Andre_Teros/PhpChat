<?php

use React\Socket\ConnectionInterface;

class ConnectionPool
{
    protected $connections;

    public function __construct()
    {
        $this->connections = new SplObjectStorage();
    }

    public function add(ConnectionInterface $connection)
    {
        $connection->write("Hello\n");

        $connection->write("Enter you name: ");
        $this->setConnectionName($connection, '');

        $connection->on('data', function ($data) use ($connection) {
            $name = $this->getConnectionName($connection);
            if (empty($name)) {
                $this->addNewMember($connection, $data);
            } else {
                $this->notyMembers($connection, "$name: $data");
            }
        });

        $connection->on('close', function () use ($connection) {
            $name = $this->getConnectionName($connection);

            $this->connections->offsetUnset($connection);

            $this->notyMembers($connection, "User $name leaves the chat\n");
        });
    }

    protected function notyMembers(ConnectionInterface $currentConnection, $message = '')
    {
        foreach ($this->connections as $conn) {
            if ($conn != $currentConnection) {
                $conn->write($message);
            }
        }
    }

    protected function addNewMember(ConnectionInterface $connection, $name)
    {
        $name = str_replace(["\n", "\r"], '', trim($name));
        $this->setConnectionName($connection, $name);
        $this->notyMembers($connection, "User $name enters the chat\n");
    }


    protected function getConnectionName(ConnectionInterface $connection)
    {
        return $this->connections->offsetGet($connection);
    }

    protected function setConnectionName(ConnectionInterface $connection, $name)
    {
        $this->connections->offsetSet($connection, $name);
    }
}